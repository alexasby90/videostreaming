# Video Streaming Application

This is a Video Streaming Application that allows users to publish, view, and manage videos. It provides a set of APIs to interact with videos and user actions.

## Table of Contents

- [Features](#features)
- [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Setting Up the Database](#setting-up-the-database)
    - [Running the Application](#running-the-application)
- [API Documentation](#api-documentation)
    - [Swagger UI](#swagger-api)    
- [Docker Compose](#docker-compose)
- [Contributing](#contributing)
- [License](#license)

## Features

- Publish videos
- View video details
- Get video playback URLs
- Search videos by director
- Soft delete videos
- Record user actions

## Getting Started

### Prerequisites

Before running the application, make sure you have the following prerequisites installed:

- Java Development Kit (JDK) 17 or higher
- Maven
- Docker
- PostgreSQL

### Setting Up the Database


- This application uses a PostgreSQL database. You can set up the database using the provided `docker-compose.yaml` file. To start the database, run the following command in the project directory:

```bash
docker-compose up
```

### Running the Application

1. Clone the repository.
2. Build the application using Maven:

 ```bash
   mvn clean install
   ```

3. Start the application:
```bash
mvn spring-boot:run
```

4. The application will be available at `http://localhost:8080`.

## API Documentation

### Swagger API

The API documentation for this application is available using Swagger. You can access it at the following URLs:

- JSON API documentation: `http://localhost:8080/videostreaming/api-docs`
- Swagger UI: `http://localhost:8080/videostreaming/swagger-ui.html`

## Docker Compose

A `docker-compose.yaml` file is provided to configure the PostgreSQL database. To start the database, you need to go to the folder where the `docker-compose.yaml` file is stored and
run the following command:

```shell
docker-compose up
```
## Contributing
Contributions are welcome! Feel free to submit issues and pull requests.

## License
This project is licensed under the MIT License.