package com.example.videostreaming.dto;

import lombok.Data;

import java.util.Set;

/**
 * Data Transfer Object (DTO) representing video information for responses.
 */
@Data
public class VideoResponseDTO {

    private String title;
    private String genre;
    private String director;
    private Long runningTime;
    private Set<String> castMembers;
}
