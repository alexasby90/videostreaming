package com.example.videostreaming.dto;

import com.example.videostreaming.model.ActionType;
import lombok.Data;

import java.time.LocalDateTime;
@Data
public class UserActionDTO {

    private Long videoId;
    private Long userId;
    private ActionType actionType;
    private LocalDateTime timestamp;
}
