package com.example.videostreaming.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.Set;

/**
 * Data Transfer Object (DTO) representing video information for requests.
 */
@Data
public class VideoRequestDTO {

    private String title;
    private String synopsis;
    private String genre;
    private String director;
    private LocalDate dateOfRelease;
    private boolean isDeleted;
    private Long runningTime;
    private Set<String> castMembers;

}
