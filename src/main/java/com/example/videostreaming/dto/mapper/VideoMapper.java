package com.example.videostreaming.dto.mapper;

import com.example.videostreaming.dto.VideoRequestDTO;
import com.example.videostreaming.dto.VideoResponseDTO;
import com.example.videostreaming.model.Video;
import org.mapstruct.Mapper;

/**
 * Mapper interface for mapping between VideoDTO and Video entities using MapStruct.
 */

@Mapper(componentModel = "spring")
public interface VideoMapper {


    Video toEntity(VideoRequestDTO videoRequestDTO);

    VideoResponseDTO toDTO(Video video);
}
