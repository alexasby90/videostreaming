package com.example.videostreaming.dto.mapper;

import com.example.videostreaming.dto.UserActionDTO;
import com.example.videostreaming.model.UserAction;
import org.mapstruct.Mapper;

/**
 * Mapper interface for mapping between UserActionDTO and UserAction entities using MapStruct.
 */
@Mapper(componentModel = "spring")
public interface UserActionMapper {

    //    @Mapping(target = "id", ignore = true)
    UserAction toEntity(UserActionDTO userActionDTO);

    UserActionDTO toDTO(UserAction userAction);

}
