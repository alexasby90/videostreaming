package com.example.videostreaming.exception;

public class VideoNotFoundException extends RuntimeException {

    private static final String message = "The requested video has not been found (id = %s)";

    public VideoNotFoundException(Long id) {
        super(String.format(message, id));
    }
}
