package com.example.videostreaming.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class VideoStreamingExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({VideoNotFoundException.class})
    public ResponseEntity<Object> handleAccessDeniedException(Exception ex) {
        return new ResponseEntity<Object>(
                ex.getMessage(), HttpStatus.NOT_FOUND);
    }
}
