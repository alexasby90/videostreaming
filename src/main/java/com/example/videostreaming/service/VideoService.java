package com.example.videostreaming.service;

import com.example.videostreaming.dto.VideoRequestDTO;
import com.example.videostreaming.dto.VideoResponseDTO;
import com.example.videostreaming.model.Video;

import java.util.List;


public interface VideoService {

    VideoResponseDTO publishVideo(VideoRequestDTO video);

    void softDeleteVideo(Long videoId);

    VideoResponseDTO getVideoById(Long videoId);

    List<VideoResponseDTO> getAllVideos();

    String getVideoPlaybackUrl(Long videoId);

    List<VideoResponseDTO> searchVideosByDirector(String director);
}
