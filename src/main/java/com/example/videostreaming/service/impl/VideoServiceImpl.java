package com.example.videostreaming.service.impl;

import com.example.videostreaming.dto.VideoRequestDTO;
import com.example.videostreaming.dto.VideoResponseDTO;
import com.example.videostreaming.dto.mapper.VideoMapper;
import com.example.videostreaming.exception.VideoNotFoundException;
import com.example.videostreaming.model.Video;
import com.example.videostreaming.repository.VideoRepository;
import com.example.videostreaming.service.VideoService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class VideoServiceImpl implements VideoService {

    private final VideoRepository videoRepository;
    private final VideoMapper videoMapper;

    @Value("${video.streaming.playbackURL}")
    private String playbackURL;

    @Override
    public VideoResponseDTO publishVideo(VideoRequestDTO videoRequestDTO) {
        Video video = videoMapper.toEntity(videoRequestDTO);
        return videoMapper.toDTO(videoRepository.save(video));
    }

    @Override
    public void softDeleteVideo(Long videoId) {
        videoRepository.findById(videoId)
                .ifPresent(video -> {
                    video.setDeleted(true);
                    videoRepository.save(video);
                });
    }

    @Override
    public VideoResponseDTO getVideoById(Long videoId) {
        return videoRepository.findById(videoId)
                .map(videoMapper::toDTO)
                .orElseThrow(() -> new VideoNotFoundException(videoId));
    }

    @Override
    public List<VideoResponseDTO> getAllVideos() {
        return videoRepository.findAll().stream()
                .map(videoMapper::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public String getVideoPlaybackUrl(Long videoId) {
        return videoRepository.findById(videoId)
                .map(video -> playbackURL + videoId + ".mp4")
                .orElseThrow(() -> new VideoNotFoundException(videoId));
    }

    @Override
    public List<VideoResponseDTO> searchVideosByDirector(String director) {
        return videoRepository.findByDirector(director).stream()
                .map(videoMapper::toDTO)
                .collect(Collectors.toList());
    }
}
