package com.example.videostreaming.service.impl;

import com.example.videostreaming.dto.UserActionDTO;
import com.example.videostreaming.dto.mapper.UserActionMapper;
import com.example.videostreaming.exception.VideoNotFoundException;
import com.example.videostreaming.model.UserAction;
import com.example.videostreaming.model.Video;
import com.example.videostreaming.repository.UserActionRepository;
import com.example.videostreaming.repository.VideoRepository;
import com.example.videostreaming.service.UserActionService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UserActionServiceImpl implements UserActionService {

    private final UserActionRepository userActionRepository;
    private final VideoRepository videoRepository;
    private final UserActionMapper userActionMapper;

    public UserActionDTO createUserAction(UserActionDTO userActionDTO) {
        Video video = videoRepository.findById(userActionDTO.getVideoId())
                .orElseThrow(() -> new VideoNotFoundException(userActionDTO.getVideoId()));

        UserAction userAction = userActionMapper.toEntity(userActionDTO);
        userAction.setVideo(video);
        UserAction savedUserAction = userActionRepository.save(userAction);

        return userActionMapper.toDTO(savedUserAction);
    }

    public List<UserActionDTO> getUserActionsByUserId(Long userId) {
        return userActionRepository.findByUserId(userId).stream()
                .map(userActionMapper::toDTO)
                .collect(Collectors.toList());
    }
}
