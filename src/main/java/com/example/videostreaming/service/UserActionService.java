package com.example.videostreaming.service;

import com.example.videostreaming.dto.UserActionDTO;
import com.example.videostreaming.model.UserAction;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service for managing user actions related to videos.
 */
@Service
public interface UserActionService {

    UserActionDTO createUserAction(UserActionDTO userActionDTO);
    List<UserActionDTO> getUserActionsByUserId(Long userId);
}
