package com.example.videostreaming.model;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;
import java.util.Set;

/**
 * Represents video content and its associated metadata.
 */

@Data
@Entity
@Table(name = "video")
public class Video {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String title;

    @Column(length = 1000)
    private String synopsis;

    @Column(nullable = false)
    private String director;

    @Column(nullable = false)
    private String genre;

    @Column(nullable = false)
    private LocalDate dateOfRelease;

    @Column(name = "running_time", nullable = false)
    private Long runningTime;

    @Column (name = "is_deleted" )
    private boolean isDeleted;

    @ElementCollection
    @CollectionTable(name = "video_cast", joinColumns = @JoinColumn(name = "video_cast_id"))
    private Set<String> castMembers;

}
