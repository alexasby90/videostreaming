package com.example.videostreaming.model;

/**
 * Enumeration representing the types of user actions related to video content.
 */
public enum ActionType {

    VIEW, // User viewed a video.
    IMPRESSIONS, // User uploaded a video.

}
