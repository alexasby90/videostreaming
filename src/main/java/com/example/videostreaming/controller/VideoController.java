package com.example.videostreaming.controller;

import com.example.videostreaming.dto.VideoRequestDTO;
import com.example.videostreaming.dto.VideoResponseDTO;
import com.example.videostreaming.service.VideoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("api/v1/videos")
@Tag(name = "Video Controller", description = "API for managing videos")
public class VideoController {

    private final VideoService videoService;

    @Operation(summary = "Publish video")
    @PostMapping
    public ResponseEntity<VideoResponseDTO> publishVideo(@RequestBody VideoRequestDTO videoRequestDTO) {
        VideoResponseDTO publishedVideo = videoService.publishVideo(videoRequestDTO);
        return ResponseEntity.ok(publishedVideo);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get video by ID")
    public ResponseEntity<VideoResponseDTO> getVideo(@PathVariable Long id) {
        VideoResponseDTO videoDTO = videoService.getVideoById(id);
        return ResponseEntity.ok(videoDTO);

    }

    @GetMapping("/{id}/play")
    @Operation(summary = "Get link for playing videos")
    public ResponseEntity<String> playVideo(@PathVariable Long id) {
        String videoUrl = videoService.getVideoPlaybackUrl(id);
        return ResponseEntity.ok(videoUrl);

    }

    @GetMapping("/director/{director}")
    @Operation(summary = "Search videos by director")
    public ResponseEntity<List<VideoResponseDTO>> searchVideosByDirector(@PathVariable String director) {
        List<VideoResponseDTO> videos = videoService.searchVideosByDirector(director);
        return ResponseEntity.ok(videos);

    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete video by ID")
    public ResponseEntity<Void> softDeleteVideo(@PathVariable Long id) {
        videoService.softDeleteVideo(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    @Operation(summary = "Get a list of all videos")
    public ResponseEntity<List<VideoResponseDTO>> getAllVideos() {
        List<VideoResponseDTO> videoResponseDTOList = videoService.getAllVideos();
        return ResponseEntity.ok(videoResponseDTOList);
    }
}

