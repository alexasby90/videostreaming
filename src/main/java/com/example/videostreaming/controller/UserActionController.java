package com.example.videostreaming.controller;

import com.example.videostreaming.dto.UserActionDTO;
import com.example.videostreaming.service.UserActionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/user-actions")
@AllArgsConstructor
@Tag(name = "User-actions Controller", description = "API for managing actions of user")
public class UserActionController {

    private final UserActionService userActionService;

    @PostMapping
    @Operation(summary = "Create user action")
    public ResponseEntity<UserActionDTO> createUserAction(@RequestBody UserActionDTO userActionDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(userActionService.createUserAction(userActionDTO));
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get user actions by user ID")
    public ResponseEntity<List<UserActionDTO>> getUserActionsByUserId(@PathVariable Long id) {
        return ResponseEntity.ok(userActionService.getUserActionsByUserId(id));
    }
}
