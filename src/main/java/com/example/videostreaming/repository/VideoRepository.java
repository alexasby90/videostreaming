package com.example.videostreaming.repository;

import com.example.videostreaming.model.Video;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VideoRepository extends JpaRepository<Video, Long> {

    List<Video> findByDirector(String director);
}
