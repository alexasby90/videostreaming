package com.example.videostreaming.configuration;

import com.example.videostreaming.dto.mapper.UserActionMapper;
import com.example.videostreaming.dto.mapper.VideoMapper;
import org.mapstruct.factory.Mappers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfig {
    @Bean
    public VideoMapper videoMapper() {
        return Mappers.getMapper(VideoMapper.class);
    }

    @Bean
    public UserActionMapper userActionMapper() {
        return Mappers.getMapper(UserActionMapper.class);
    }
}
