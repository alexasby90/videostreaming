package com.example.videostreaming.service.impl.service;

import com.example.videostreaming.dto.VideoRequestDTO;
import com.example.videostreaming.dto.VideoResponseDTO;
import com.example.videostreaming.dto.mapper.VideoMapper;
import com.example.videostreaming.exception.VideoNotFoundException;
import com.example.videostreaming.model.Video;
import com.example.videostreaming.repository.VideoRepository;
import com.example.videostreaming.service.impl.VideoServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class VideoServiceImplTest {

    @Mock
    private VideoRepository videoRepository;

    @Mock
    private VideoMapper videoMapper;

    @InjectMocks
    private VideoServiceImpl videoService;

    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField( videoService, "playbackURL", "http://example.com/videos/");
    }

    @Test
    void shouldPublishVideo() {
        VideoRequestDTO requestDTO = new VideoRequestDTO();
        Video video = new Video();
        VideoResponseDTO responseDTO = new VideoResponseDTO();

        when(videoMapper.toEntity(requestDTO)).thenReturn(video);
        when(videoMapper.toDTO(video)).thenReturn(responseDTO);
        when(videoRepository.save(video)).thenReturn(video);

        VideoResponseDTO result = videoService.publishVideo(requestDTO);

        assertNotNull(result);
        assertEquals(responseDTO, result);
        verify(videoRepository, times(1)).save(video);
    }

    @Test
    void shouldSoftDeleteVideo() {
        Long videoId = 1L;
        Video video = new Video();
        video.setDeleted(false);

        when(videoRepository.findById(videoId)).thenReturn(Optional.of(video));
        when(videoRepository.save(video)).thenReturn(video);

        videoService.softDeleteVideo(videoId);

        assertTrue(video.isDeleted());
        verify(videoRepository, times(1)).save(video);
    }

    @Test
    void shouldGetVideoById() {
        Long videoId = 1L;
        Video video = new Video();
        VideoResponseDTO responseDTO = new VideoResponseDTO();

        when(videoRepository.findById(videoId)).thenReturn(Optional.of(video));
        when(videoMapper.toDTO(video)).thenReturn(responseDTO);

        VideoResponseDTO result = videoService.getVideoById(videoId);

        assertNotNull(result);
        assertEquals(responseDTO, result);
    }

    @Test
    void shouldGetAllVideos() {
        List<Video> videos = new ArrayList<>();
        List<VideoResponseDTO> responseDTOs = new ArrayList<>();

        when(videoRepository.findAll()).thenReturn(videos);
        when(videoMapper.toDTO(any(Video.class))).thenReturn(new VideoResponseDTO());

        List<VideoResponseDTO> result = videoService.getAllVideos();

        assertNotNull(result);
        assertEquals(responseDTOs.size(), result.size());
    }

    @Test
    void shouldGetVideoPlaybackUrl() {
        Long videoId = 1L;
        Video video = new Video();
        String playbackURL = "http://example.com/videos/";

        when(videoRepository.findById(videoId)).thenReturn(Optional.of(video));

        String result = videoService.getVideoPlaybackUrl(videoId);

        assertNotNull(result);
        assertEquals(playbackURL + videoId + ".mp4", result);
    }

    @Test
    public void testGetVideoPlaybackUrlForNonExistingVideo() {
        when(videoRepository.findById(anyLong())).thenThrow(VideoNotFoundException.class);
        assertThrows(VideoNotFoundException.class, () -> videoRepository.findById(anyLong()));
    }

    @Test
    void searchVideosByDirector() {

        Video video1 = new Video();
        Video video2 = new Video();
        when(videoRepository.findByDirector("DirectorName")).thenReturn(Arrays.asList(video1, video2));

        VideoResponseDTO videoResponseDTO1 = new VideoResponseDTO();
        VideoResponseDTO videoResponseDTO2 = new VideoResponseDTO();
        when(videoMapper.toDTO(video1)).thenReturn(videoResponseDTO1);
        when(videoMapper.toDTO(video2)).thenReturn(videoResponseDTO2);

        List<VideoResponseDTO> videos = videoService.searchVideosByDirector("DirectorName");
        assertEquals(2, videos.size());
    }
}