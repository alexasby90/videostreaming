package com.example.videostreaming.service.impl.controller;

import com.example.videostreaming.controller.UserActionController;
import com.example.videostreaming.dto.UserActionDTO;
import com.example.videostreaming.service.UserActionService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
public class UserActionControllerTest {

    @Mock
    private UserActionService userActionService;

    @InjectMocks
    private UserActionController userActionController;

    @Test
    void testCreateUserAction() {
        UserActionDTO userActionDTO = new UserActionDTO();

        Mockito.when(userActionService.createUserAction(userActionDTO)).thenReturn(userActionDTO);

        ResponseEntity<UserActionDTO> response = userActionController.createUserAction(userActionDTO);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(userActionDTO, response.getBody());
    }

    @Test
    void testGetUserActionsByUserId() {
        Long userId = 1L;
        List<UserActionDTO> userActions = Collections.singletonList(new UserActionDTO());

        Mockito.when(userActionService.getUserActionsByUserId(userId)).thenReturn(userActions);

        ResponseEntity<List<UserActionDTO>> response = userActionController.getUserActionsByUserId(userId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(userActions, response.getBody());
    }
}
