package com.example.videostreaming.service.impl.controller;

import com.example.videostreaming.controller.VideoController;
import com.example.videostreaming.dto.VideoRequestDTO;
import com.example.videostreaming.dto.VideoResponseDTO;
import com.example.videostreaming.service.VideoService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class VideoControllerTest {

    @Mock
    private VideoService videoService;
    @InjectMocks
    private VideoController videoController;

    @Test
    void shouldPublishVideoTest() {
        VideoRequestDTO requestDTO = new VideoRequestDTO();
        VideoResponseDTO responseDTO = new VideoResponseDTO();

        when(videoService.publishVideo(requestDTO)).thenReturn(responseDTO);

        ResponseEntity<VideoResponseDTO> response = videoController.publishVideo(requestDTO);

        assertEquals(responseDTO, response.getBody());
    }

    @Test
    void shouldGetVideoTest() {
        Long videoId = 1L;
        VideoResponseDTO responseDTO = new VideoResponseDTO();

        when(videoService.getVideoById(videoId)).thenReturn(responseDTO);

        ResponseEntity<VideoResponseDTO> response = videoController.getVideo(videoId);

        assertEquals(responseDTO, response.getBody());
    }

    @Test
    void shouldPlayVideoTest() {
        Long videoId = 1L;
        String videoUrl = "http://example.com/videos/1.mp4";

        when(videoService.getVideoPlaybackUrl(videoId)).thenReturn(videoUrl);

        ResponseEntity<String> response = videoController.playVideo(videoId);

        assertEquals(videoUrl, response.getBody());
    }

    @Test
    void searchVideosByDirectorTest() {
        String director = "Director Name";
        List<VideoResponseDTO> videoList = Collections.singletonList(new VideoResponseDTO());

        when(videoService.searchVideosByDirector(director)).thenReturn(videoList);

        ResponseEntity<List<VideoResponseDTO>> response = videoController.searchVideosByDirector(director);

        assertEquals(videoList, response.getBody());
    }

    @Test
    void softDeleteVideoTest() {
        Long videoId = 1L;

        ResponseEntity<Void> response = videoController.softDeleteVideo(videoId);

        assertEquals(204, response.getStatusCode().value());
    }

    @Test
    void getAllVideosTest() {
        List<VideoResponseDTO> videoList = Collections.singletonList(new VideoResponseDTO());

        when(videoService.getAllVideos()).thenReturn(videoList);

        ResponseEntity<List<VideoResponseDTO>> response = videoController.getAllVideos();

        assertEquals(videoList, response.getBody());
    }
}
