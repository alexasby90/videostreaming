package com.example.videostreaming.service.impl.service;

import com.example.videostreaming.dto.UserActionDTO;
import com.example.videostreaming.dto.mapper.UserActionMapper;
import com.example.videostreaming.exception.VideoNotFoundException;
import com.example.videostreaming.model.UserAction;
import com.example.videostreaming.model.Video;
import com.example.videostreaming.repository.UserActionRepository;
import com.example.videostreaming.repository.VideoRepository;
import com.example.videostreaming.service.impl.UserActionServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class UserActionServiceImplTest {

    @Mock
    private UserActionRepository userActionRepository;

    @Mock
    private VideoRepository videoRepository;

    @Mock
    private UserActionMapper userActionMapper;

    @InjectMocks
    private UserActionServiceImpl userActionService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        userActionService = new UserActionServiceImpl(userActionRepository, videoRepository, userActionMapper);
    }


    @Test
    void shouldCreateUserAction() {
        UserActionDTO userActionDTO = new UserActionDTO();
        userActionDTO.setVideoId(1L);

        Video video = new Video();
        video.setId(1L);

        when(videoRepository.findById(userActionDTO.getVideoId())).thenReturn(Optional.of(video));
        when(userActionMapper.toEntity(userActionDTO)).thenReturn(new UserAction());

        UserAction savedUserAction = new UserAction();

        when(userActionRepository.save(Mockito.any(UserAction.class))).thenReturn(savedUserAction);
        when(userActionMapper.toDTO(any(UserAction.class))).thenReturn(userActionDTO);

        UserActionDTO result = userActionService.createUserAction(userActionDTO);

        assertEquals(userActionDTO, result);
    }

    @Test
    void shouldCreateUserActionWithNonExistingVideo() {
        UserActionDTO userActionDTO = new UserActionDTO();
        userActionDTO.setVideoId(1L);

        when(videoRepository.findById(userActionDTO.getVideoId())).thenReturn(Optional.empty());

        assertThrows(VideoNotFoundException.class, () -> userActionService.createUserAction(userActionDTO));
    }

    @Test
    void shouldGetUserActionsByUserId() {
        Long userId = 1L;
        List<UserAction> userActions = Collections.singletonList(new UserAction());

        when(userActionRepository.findByUserId(userId)).thenReturn(userActions);
        when(userActionMapper.toDTO(Mockito.any(UserAction.class)))
                .thenAnswer(invocation -> {
                    UserAction action = invocation.getArgument(0);
                    UserActionDTO dto = new UserActionDTO();
                    return dto;
                });

        List<UserActionDTO> result = userActionService.getUserActionsByUserId(userId);

        assertEquals(userActions.size(), result.size());
    }
}
